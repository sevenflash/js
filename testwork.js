// Задача: вывести значения массива с заданным между ними интервалом времени без использования setInterval
var array = ["a","b","c","d","e"];
var delay = 1000;

// 0 - setInterval
var currentIndex = 0;
setInterval(function() {
    console.log(array[currentIndex]);
    if(++currentIndex>=array.length) {
        clearInterval(this);
    }
}, delay);

// 1 - setTimeout в контексте setDelay
function setDelay(currentIndex) {
    setTimeout(function() {
        console.log(array[currentIndex]);
    }, delay*currentIndex);
}
for(var currentIndex=0; currentIndex<array.length; currentIndex++) {
    setDelay(currentIndex);
}

// 2 - setTimeout в контексте анонимной функции
for(var currentIndex=0; currentIndex<array.length; currentIndex++) {
    (function(index) {
        setTimeout(function() {
            console.log(array[index]);
        }, delay*index);
    })(currentIndex);
}

// 3 - Наилучший вариант с использованием цикла
array.forEach(function(value, currentIndex) {
    setTimeout(function() {
        console.log(value);
    }, delay*currentIndex);
});

// 4 - Замыкание внутри callback
for(var currentIndex=0; currentIndex<array.length; currentIndex++) {
    setTimeout(function(index) {
        return function() {
            console.log(array[index]);
        };
    }(currentIndex), delay*(currentIndex));
}