var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');

gulp.task('css', function () {
    return gulp.src('src/style/**/*.css')
        .pipe(minifyCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('/'));
});

gulp.task('js', function() {
    gulp.src('src/js/**/*.css')
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('/'))
});

gulp.task('default', ['css', 'js']);