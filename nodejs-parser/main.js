var request = require('request');
var cheerio = require('cheerio');

var url = 'https://ru.wikipedia.org';
var numberLinks = 0;
var maxLinks = 1000;

request(url, function (error, response, body) {
    if (error) return;
    var $ = cheerio.load(body);
    var links = $('a');
    links.each(function () {
        var path = $(this).attr('href');
        request(url+path, function (error, response, body) {
            if (error) return;
            if ((++numberLinks) > maxLinks) return;
            if(path.indexOf('http')==0) return;
            var $ = cheerio.load(body);
            var links = $('a');
            links.each(function () {
                var path = $(this).attr('href');
                if(path!=undefined&&path.indexOf('/')==0&&path.indexOf('//')!=0) {
                    numberLinks++;
                    console.log(url + path);
                }
            });
        });
    });
});